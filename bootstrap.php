<?php

/**
 * BOOTSTRAP
 */
use R3C\Wordpress\Router\Router;
use R3C\Wordpress\EventDispatcher\EventDispatcher;

if (file_exists('config/routes.php')) {
    $router = new Router();
    include_once 'config/routes.php';

    EventDispatcher::addListener(function() use ($router) {
        $router->checkAndRun();
    });
}