<?php
namespace R3C\Wordpress\Router\Controllers;

interface ResourceControllerInterface
{
	//A index é uma listagem da entidade, com ou sem filtro
    public function index();

    //As funções abaixo são utilizadas para mostrar formulários
    public function create();
    public function edit($id);

    //As funções abaixo são utilizadas para respostas via AJAX
    public function destroy($id);
    public function show($id);
    public function store();
    public function update($id);
}