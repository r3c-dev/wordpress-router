<?php
namespace R3C\Wordpress\Router;

use Exception;
use Symfony\Component\HttpFoundation\Request;
use ReflectionMethod;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class Router
{
	public $error = '';
	private $routes;
	private $request;
	private $parameters;

	public function __construct()
	{
		$this->routes = new RouteCollection();

		$this->request = Request::createFromGlobals();
	}

	public function addRoute($route, $controller, $methods = array('POST', 'GET'))
	{
		$firstCharacter = substr($route, 0, 1);

		if ($firstCharacter != '/') {
			$route = '/' . $route;
		}

		$routeObject = new Route($route, array('controller' => $controller));

		if ( ! is_array($methods)) {
			$methods = [$methods];
		}

		//A rota precisa ter um nome único, por isso é gerada a variável $unique
		$unique = '';

		foreach ($methods as $method) {
			$unique .= $method;
		}

		$routeObject->setMethods($methods);

		$this->routes->add($route . $unique, $routeObject);
	}

	public function addResource($route, $controller)
	{
		//index, create, store, show, edit, update, destroy

		//GET 	/resource 	index
		$this->addRoute($route, $controller . '@index', 'GET');
		$this->addRoute($route . '/', $controller . '@index', 'GET');

		//GET 	/resource/create 	create
		$this->addRoute($route . '/create', $controller . '@create', 'GET');
		$this->addRoute($route . '/create/', $controller . '@create', 'GET');

		//POST 	/resource 	store
		$this->addRoute($route, $controller . '@store', 'POST');
		$this->addRoute($route . '/', $controller . '@store', 'POST');

		//GET 	/resource/{resource} 	show
		$this->addRoute($route . '/{id}', $controller . '@show', 'GET');
		$this->addRoute($route . '/{id}/', $controller . '@show', 'GET');

		//GET 	/resource/{resource}/edit 	edit
		$this->addRoute($route . '/{id}/edit', $controller . '@edit', 'GET');
		$this->addRoute($route . '/{id}/edit/', $controller . '@edit', 'GET');

		//PUT/PATCH 	/resource/{resource} 	update
		$this->addRoute($route . '/{id}', $controller . '@update', 'PUT');
		$this->addRoute($route . '/{id}/', $controller . '@update', 'PUT');
		$this->addRoute($route . '/{id}', $controller . '@update', 'PATCH');
		$this->addRoute($route . '/{id}/', $controller . '@update', 'PATCH');

		//DELETE 	/resource/{resource} 	destroy
		$this->addRoute($route . '/{id}', $controller . '@destroy', 'DELETE');
		$this->addRoute($route . '/{id}/', $controller . '@destroy', 'DELETE');
	}

	public function check()
	{
		$context = new RequestContext();
		$context->fromRequest($this->request);

		$matcher = new UrlMatcher($this->routes, $context);

		try {
			$this->parameters = $matcher->matchRequest($this->request);
		} catch (Exception $e) {
			return false;
		}

		return true;
	}

	public function checkAndRun()
	{
		if ($this->check()) {
			define( 'DOING_AJAX', true );

			if ( ! defined('WP_ADMIN')) {
				define('WP_ADMIN', true);
			}

			/** Load WordPress Bootstrap */
			require_once('wp/wp-load.php');

			$this->run();

			die();
		}
	}

	public function run()
	{
		$parameters = $this->parameters;

		if ( ! array_key_exists('_route', $parameters) || ! array_key_exists('controller', $parameters)) {
			return false;
		}

		$route = $parameters['_route'];
		$controllerExplode = explode('@', $parameters['controller']);

		$controllerName = $controllerExplode[0];
		$functionName = $controllerExplode[1];

		$reflectionMethod = new ReflectionMethod($controllerName, $functionName);
		$methodParameters = $reflectionMethod->getParameters();

		$parametersArray = [];
		foreach ($methodParameters as $key => $item) {
			$parameterName = $item->getName();
			$parametersArray[] = $parameters[$parameterName];
		}

		$controller = new $controllerName;
		echo $reflectionMethod->invokeArgs($controller, $parametersArray);
	}
}