## R3C Wordpress Router

Classe de mapeamento de rotas HTTP

# Adicione uma rota manualmente #


```
#!php

$router->addRoute('/teste', 'Teste\Controller\TesteController@index', 'GET');
```


# Adicione um recurso REST #


```
#!php

$router->addResource('/teste', 'Teste\Controller\TesteController');
```


Nesse caso, o Controller deve implementar a interface ResourceControllerInterface:


```
#!php
use R3C\Wordpress\Router\Controllers\ResourceControllerInterface;

class TesteController implements ResourceControllerInterface
{
    //A index é uma listagem da entidade, com ou sem filtro
    public function index()
    {
    }

    //As funções abaixo são utilizadas para mostrar formulários
    public function create()
    {
    }

    public function edit($id)
    {
    }

    //As funções abaixo são utilizadas para respostas via AJAX
    public function destroy($id)
    {
    }

    public function show($id)
    {
    }

    public function store()
    {
    }

    public function update($id)
    {
    }
}
```
